//============================================================================
// Name        : arfcntofreq.cpp
// Author      : Alex Munroe
// Version     : 0.2
// Copyright   : � Alex Munroe 2013 | De Montfort University
// Description : ARFCN TO RF Calculator
//============================================================================

#include <iostream>
using namespace std;

int main() {
	int arfcn;
	float upfreq, downfreq;

	cout << "Please Enter the ARFCN you want to calculate the frequency for: ";
	cin >> arfcn;

	if((arfcn == 0) || (arfcn==974))
	{
		cout << "\nWARNING\n\n";
		cout << "Band " << arfcn << " is reserved as a guard band and is not usable for traffic\n";
		cout << "Frequency 890.0 MHz\n" << endl;
	}
	else if((arfcn >= 1) && (arfcn <= 124))
	{
		upfreq = 890.0 + (arfcn *.2);
		downfreq = upfreq + 45.0;
		cout << "Frequency Type: GSM900\n";
		cout << "Uplink Frequency " << upfreq << "MHz\n";
		cout << "Downlink Frequency " << downfreq << "MHz" << endl;
	}
	else if((arfcn >= 975) && (arfcn <= 1023))
	{
		upfreq = 890.0 + ((arfcn-1024) *.2);
		downfreq = upfreq + 45.0;
		cout << "Frequency Type: GSM900\n";
		cout << "Uplink Frequency " << upfreq << "MHz\n";
		cout << "Downlink Frequency " << downfreq << "MHz" << endl;
	}
	else if((arfcn >= 512) && (arfcn <= 885))
	{
		upfreq = 1710.0 + ((arfcn-511) *.2);
		downfreq = upfreq + 95.0;
		cout << "Frequency Type: GSM1800 (DCS1800)\n";
		cout << "Uplink Frequency " << upfreq << "MHz\n";
		cout << "Downlink Frequency " << downfreq << "MHz" << endl;
	}
	else
	{
		cout << "An incorrect ARFCN has been entered\n";
		cout << "Please note this calculator can only be used for GSM frequencies\n(GSM900/GSM1800)\n";
		cout << "PCS1900 and other non-standard frequencies are not calculated" << endl;
	}
	return 0;
}
